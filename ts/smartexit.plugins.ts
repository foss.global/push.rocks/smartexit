// node native
import * as childProcess from 'child_process';

export { childProcess };

// pushrocks scope
import * as lik from '@pushrocks/lik';
import * as smartdelay from '@pushrocks/smartdelay';

export { lik, smartdelay };
