import * as plugins from './smartexit.plugins';

export class SmartExit {
  public processesToEnd = new plugins.lik.ObjectMap<plugins.childProcess.ChildProcess>();
  public cleanupFunctions = new plugins.lik.ObjectMap<() => Promise<any>>();

  /**
   * adds a process to be exited
   * @param childProcessArg
   */
  public addProcess(childProcessArg: plugins.childProcess.ChildProcess) {
    this.processesToEnd.add(childProcessArg);
  }

  public addCleanupFunction(cleanupFunctionArg: () => Promise<any>) {
    this.cleanupFunctions.add(cleanupFunctionArg);
  }

  /**
   * removes a process to be exited
   */
  public removeProcess(childProcessArg: plugins.childProcess.ChildProcess) {
    this.processesToEnd.remove(childProcessArg);
  }

  public async killAll() {
    console.log('Checking for remaining child processes before exit...');
    if (this.processesToEnd.getArray().length > 0) {
      console.log('found remaining child processes');
      let counter = 1;
      this.processesToEnd.forEach(async (childProcessArg) => {
        const pid = childProcessArg.pid;
        console.log(`killing process #${counter} with pid ${pid}`);
        plugins.smartdelay.delayFor(10000).then(() => {
          if (childProcessArg.killed) {
            return;
          }
          process.kill(pid, 'SIGKILL');
        });
        process.kill(pid, 'SIGINT');

        counter++;
      });
    } else {
      console.log(`ChildProcesses look clean.`);
    }
    if (this.cleanupFunctions.getArray.length > 0) {
      this.cleanupFunctions.forEach(async cleanupFunction => {
        await cleanupFunction();
      })
    }
    console.log(`Ready to exit!`);
  }

  constructor() {
    // do app specific cleaning before exiting
    process.on('exit', async (code) => {
      if (code === 0) {
        console.log('Process wants to exit');
        await this.killAll();
        console.log('Exited ok!');
      } else {
        console.error('Exited NOT OK!');
      }
    });

    // catch ctrl+c event and exit normally
    process.on('SIGINT', async () => {
      console.log('Ctrl-C... or SIGINT signal received!');
      await this.killAll();
      process.exit(0);
    });

    //catch uncaught exceptions, trace, then exit normally
    process.on('uncaughtException', async (err) => {
      console.log('SMARTEXIT: uncaught exception...');
      console.log(err);
      await this.killAll();
      process.exit(1);
    });
  }
}
